﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace Vacations.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VacationsForecastController : ControllerBase
    {
        private readonly ILogger<VacationsForecastController> _logger;

        public VacationsForecastController(ILogger<VacationsForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<IVacationsForecast> Get()
        {
            _logger.LogTrace("Someone called a IEnumerable<IVacationsForecast> Get()");
            return new[]
            {
                new VacationsForecast
                {
                    Date = DateTime.Now.Date
                }
            };
        }
    }
}
