using System;

namespace Vacations
{
    public class VacationsForecast : IVacationsForecast
    {
        public DateTime Date { get; set; }
    }
}
