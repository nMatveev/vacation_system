﻿using System;
using System.Collections.Generic;

namespace VacationsCore
{
    public class User : IUser
    {
        public Guid Guid { get; set; } = Guid.NewGuid();
        public List<string> Login { get; set; }
    }
}
