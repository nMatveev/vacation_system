﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VacationsCore
{
    class Vacations : IVacations
    {
        public Guid Guid { get; set; } = Guid.NewGuid();
        public List<DateTime> Dates { get; set; }
    }
}
