﻿using System;
using System.Collections.Generic;

namespace VacationsCore
{
    internal interface IVacations
    {
        Guid Guid { get; set; }
        /// <summary>
        /// Даты отпуска
        /// </summary>
        List<DateTime> Dates { get; set; }
    }
}